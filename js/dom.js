let tag1 = document.getElementsByTagName("h1");
console.log(tag1);
console.log("ini innerHTML " + tag1[0].innerHTML);
console.log("ini innerText " + tag1[0].innerText);
console.log("ini textcontent " + tag1[0].textContent);

let tag2 = document.getElementsByTagName("small");
console.log("isi tag small " + tag2[0].innerText);

let idDeskripsi = document.getElementById("deskripsi");
console.log(idDeskripsi);
console.log(`pake id = ${idDeskripsi.innerText}`);

let tanggal = document.getElementsByClassName("tanggal");
console.log(tanggal);

let dataDeskripsi = document.querySelector("#deskripsi");
let dataTanggal = document.querySelector(".tanggal");
// let dataP = document.querySelectorAll("p");
let dataLi = document.querySelectorAll("li");
// console.log();

// // styling content
// dataP[0].style.background = "#dededede";
// dataP[0].style.borderBottom = "1px solid red";

let dataH1 = document.querySelector("h1");
// dataH1.classList.add("bg-warning");
dataH1.classList.toggle("bg-warning");

let dataUl = document.querySelector("ul");
// dataUl.classList.remove("list-group");

let gantiWarnaP = document.querySelectorAll("p");
gantiWarnaP.style.color = "magenta";
