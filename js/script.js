let nilaiTugasQuiz = [];
let nilaiUTS = [];
let nilaiUAS = [];
let nilaiPraktikum = [];

let akhirTugas = [];
let akhirUts = [];
let akhirUas = [];
let akhirPrak = [];

let namaForm = document.getElementById("formNama");
let tugasForm = document.getElementById("tugasForm");
let utsForm = document.getElementById("utsForm");
let uasForm = document.getElementById("uasForm");
let praktikumForm = document.getElementById("praktikumForm");

// // tugasForm.style.display = "none";
utsForm.style.display = "none";
uasForm.style.display = "none";
praktikumForm.style.display = "none";

let tabel = document.getElementById("tabelNilai");
let form = document.getElementById("form");
let submit = document.getElementById("submit");
let reset = document.getElementById("reset");

tabel.style.display = "none";
reset.style.display = "none";

// action menu
document.getElementById("menuTugas").addEventListener("click", () => {
  tugasForm.style.display = "block";
  utsForm.style.display = "none";
  uasForm.style.display = "none";
  praktikumForm.style.display = "none";
});

document.getElementById("menuUts").addEventListener("click", () => {
  tugasForm.style.display = "none";
  utsForm.style.display = "block";
  uasForm.style.display = "none";
  praktikumForm.style.display = "none";
});

document.getElementById("menuUas").addEventListener("click", () => {
  tugasForm.style.display = "none";
  utsForm.style.display = "none";
  uasForm.style.display = "block";
  praktikumForm.style.display = "none";
});

document.getElementById("menuPraktikum").addEventListener("click", () => {
  tugasForm.style.display = "none";
  utsForm.style.display = "none";
  uasForm.style.display = "none";
  praktikumForm.style.display = "block";
});

function inputNilaiTugasQuiz() {
  // ambil nilai tugas&quiz
  let nilaiTugasQuiz_1 = parseFloat(
    document.getElementById("nilaitugasQuiz_1").value
  );
  let nilaiTugasQuiz_2 = parseFloat(
    document.getElementById("nilaitugasQuiz_2").value
  );
  let nilaiTugasQuiz_3 = parseFloat(
    document.getElementById("nilaitugasQuiz_3").value
  );

  nilaiTugasQuiz.push(nilaiTugasQuiz_1, nilaiTugasQuiz_2, nilaiTugasQuiz_3);
  console.log(nilaiTugasQuiz);
  averageTugasQuiz();
}

function inputNilaiUts() {
  // ambil nilai UTS
  let uts_1 = parseFloat(document.getElementById("uts_1").value);
  let uts_2 = parseFloat(document.getElementById("uts_2").value);
  let uts_3 = parseFloat(document.getElementById("uts_3").value);

  nilaiUTS.push(uts_1, uts_2, uts_3);
  console.log(nilaiUTS);
  averageUts();
}

function inputNilaiUas() {
  // ambil nilai UAS
  let uas_1 = parseFloat(document.getElementById("uas_1").value);
  let uas_2 = parseFloat(document.getElementById("uas_2").value);
  let uas_3 = parseFloat(document.getElementById("uas_3").value);

  nilaiUAS.push(uas_1, uas_2, uas_3);
  console.log(nilaiUAS);
  averageUas();
}

function inputNilaiPraktikum() {
  //   ambil nilai praktikum
  let praktikum_1 = parseFloat(document.getElementById("prak_1").value);
  let praktikum_2 = parseFloat(document.getElementById("prak_2").value);
  let praktikum_3 = parseFloat(document.getElementById("prak_3").value);

  nilaiPraktikum.push(praktikum_1, praktikum_2, praktikum_3);
  console.log(nilaiPraktikum);
  averagePraktikum();
}

function averageTugasQuiz() {
  let total = 0;
  for (let i = 0; i < nilaiTugasQuiz.length; i++) {
    total += nilaiTugasQuiz[i];
  }
  let average = total / nilaiTugasQuiz.length;
  console.log(average);
  finalTugas(average);
}

function averageUts() {
  let total = 0;
  for (let i = 0; i < nilaiUTS.length; i++) {
    total += nilaiUTS[i];
  }
  let averageUTS = total / nilaiUTS.length;
  console.log(averageUTS);
  finalUTS(averageUTS);
}

function averageUas() {
  let total = 0;
  for (let i = 0; i < nilaiUAS.length; i++) {
    total += nilaiUAS[i];
  }
  let averageUAS = total / nilaiUAS.length;
  console.log(averageUAS);
  finalUAS(averageUAS);
}

function averagePraktikum() {
  let total = 0;
  for (let i = 0; i < nilaiPraktikum.length; i++) {
    total += nilaiPraktikum[i];
  }
  let averagePRAKTIKUM = total / nilaiPraktikum.length;
  console.log(averagePRAKTIKUM);
  finalPRAKTIKUM(averagePRAKTIKUM);
}

function finalTugas(tugas) {
  console.log("ini rata tugas ", tugas);
  let finalTugas = 0.2 * tugas;
  console.log("nilai akhir tugas & quiz ", finalTugas);
  akhirTugas.push(finalTugas);
}

function finalUTS(uts) {
  console.log("ini rata uts ", uts);
  let finalUts = 0.25 * uts;
  console.log("nilai akhir uts ", finalUts);
  akhirUts.push(finalUts);
}

function finalUAS(uas) {
  console.log("ini rata uas ", uas);
  let finalUas = 0.35 * uas;
  console.log("nilai akhir uas ", finalUas);
  akhirUas.push(finalUas);
}

function finalPRAKTIKUM(praktikum) {
  console.log("ini rata praktikum ", praktikum);
  let finalPraktikum = 0.2 * praktikum;
  console.log("nilai akhir praktikum ", finalPraktikum);
  akhirPrak.push(finalPraktikum);
}

function klik() {
  inputNilaiTugasQuiz();
  inputNilaiUts();
  inputNilaiUas();
  inputNilaiPraktikum();

  tugasForm.style.display = "none";
  utsForm.style.display = "none";
  uasForm.style.display = "none";
  praktikumForm.style.display = "none";

  namaForm.style.display = "none";
  // form.style.display = "none";
  document.getElementById("awalan").style.display = "none";
  tabel.style.display = "block";
  submit.style.display = "none";
  reset.style.display = "block";

  let indeks = akhirTugas[0] + akhirUts[0] + akhirUas[0] + akhirPrak[0];
  console.log("indeks", indeks);

  let mhs = document.getElementById("mahasiswa").value;

  if (indeks >= 80) {
    document.getElementById(
      "keterangan"
    ).innerHTML = `Mahasiswa ${mhs} diyatakan selesai dengan indeks A`;
  } else if (indeks >= 65 && indeks < 80) {
    document.getElementById(
      "keterangan"
    ).innerHTML = `Mahasiswa ${mhs} diyatakan selesai dengan indeks B`;
  } else if (indeks >= 50 && indeks < 65) {
    document.getElementById(
      "keterangan"
    ).innerHTML = `Mahasiswa ${mhs} diyatakan selesai dengan indeks C`;
  } else if (indeks >= 35 && indeks < 50) {
    document.getElementById(
      "keterangan"
    ).innerHTML = `Mahasiswa ${mhs} diyatakan selesai dengan indeks D`;
  } else if (indeks < 35) {
    document.getElementById(
      "keterangan"
    ).innerHTML = `Mahasiswa ${mhs} diyatakan selesai dengan indeks E`;
  } else {
    document.getElementById("keterangan").innerHTML = `Tidak ada nilai indeks`;
  }

  document.getElementById("hasilTugas").value = akhirTugas;
  document.getElementById("hasilUts").value = akhirUts;
  document.getElementById("hasilUas").value = akhirUas;
  document.getElementById("hasilPrak").value = akhirPrak;
  document.getElementById("hasilAkhir").value = indeks;
}

function empty() {
  nilaiTugasQuiz = [];
  nilaiUTS = [];
  nilaiUAS = [];
  nilaiPraktikum = [];

  akhirTugas = [];
  akhirUts = [];
  akhirUas = [];
  akhirPrak = [];
  document.getElementById("mahasiswa").value = "";
  document.getElementById("nilaitugasQuiz_1").value = "";
  document.getElementById("nilaitugasQuiz_2").value = "";
  document.getElementById("nilaitugasQuiz_3").value = "";
  document.getElementById("uts_1").value = "";
  document.getElementById("uts_2").value = "";
  document.getElementById("uts_3").value = "";
  document.getElementById("uas_1").value = "";
  document.getElementById("uas_2").value = "";
  document.getElementById("uas_3").value = "";
  document.getElementById("prak_1").value = "";
  document.getElementById("prak_2").value = "";
  document.getElementById("prak_3").value = "";
}

function resetBtn() {
  namaForm.style.display = "block";
  document.getElementById("awalan").style.display = "block";
  // form.style.display = "block";
  tabel.style.display = "none";
  reset.style.display = "none";
  submit.style.display = "block";
  empty();
}
